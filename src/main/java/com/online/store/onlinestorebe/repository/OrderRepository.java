package com.online.store.onlinestorebe.repository;

import com.online.store.onlinestorebe.model.Order;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;

@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {
    @Query("SELECT o FROM Order o WHERE o.orderDate BETWEEN :startDate AND :endDate")
    Page<Order> findByCreatedDateBetween(
            @Param("startDate") LocalDateTime startDate,
            @Param("endDate") LocalDateTime endDate,
            Pageable pageable
    );

    @Query("SELECT o FROM Order o WHERE o.orderDate >= :startDate")
    Page<Order> findByCreatedDateAfter(
            @Param("startDate") LocalDateTime startDate,
            Pageable pageable
    );

    @Query("SELECT o FROM Order o WHERE o.orderDate <= :endDate")
    Page<Order> findByCreatedDateBefore(
            @Param("endDate") LocalDateTime endDate,
            Pageable pageable
    );
}