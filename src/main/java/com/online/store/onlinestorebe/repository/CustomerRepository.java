package com.online.store.onlinestorebe.repository;

import com.online.store.onlinestorebe.model.Customer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {
    Optional<Customer> findByCustomerIdAndIsActive(long id, boolean isActive);
    Page<Customer> findByIsActive(boolean isActive, Pageable pageable);
    Page<Customer> findByCustomerNameContainingAndIsActive(String name, boolean isActive, Pageable pageable);
}
