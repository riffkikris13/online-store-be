package com.online.store.onlinestorebe.repository;

import com.online.store.onlinestorebe.model.Item;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ItemRepository extends JpaRepository<Item, Long> {
    Page<Item> findByIsAvailable(boolean isAvailable, Pageable pageable);
    Page<Item> findByItemNameContainingAndIsAvailable(String name, boolean isAvailable, Pageable pageable);
}
