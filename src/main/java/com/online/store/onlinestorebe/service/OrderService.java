package com.online.store.onlinestorebe.service;

import com.online.store.onlinestorebe.dto.*;
import com.online.store.onlinestorebe.exception.DataNotFoundException;
import com.online.store.onlinestorebe.model.Customer;
import com.online.store.onlinestorebe.model.Item;
import com.online.store.onlinestorebe.model.Order;
import com.online.store.onlinestorebe.repository.CustomerRepository;
import com.online.store.onlinestorebe.repository.OrderRepository;
import io.minio.PutObjectArgs;
import io.minio.RemoveObjectArgs;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class OrderService {
    private OrderRepository orderRepository;

    private CustomerRepository customerRepository;

    public OrderRes createOrder(OrderReq orderReq) throws DataNotFoundException {
        Order order = reqConverter(orderReq);
        LocalDateTime dateNow = LocalDateTime.now();
        order.setOrderCode("ORD-" + UUID.randomUUID().toString().substring(0, 13));
        order.setOrderDate(dateNow);

        customerRepository.findById(order.getCustomer().getCustomerId())
                .map(customer -> {
                    customer.setLastOrderDate(dateNow);
                    customerRepository.save(customer);
                    return customer;
                })
                .orElseThrow(() -> new DataNotFoundException("Customer not found"));

        return resConverter(orderRepository.save(order));
    }

    public OrderRes getOrder(Long id) throws DataNotFoundException {
        return resConverter(orderRepository.findById(id).orElseThrow(() -> new DataNotFoundException("Order not found")));
    }

    public OrderPagingRes getOrders(String startDate, String endDate, int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        Page<Order> pageData;
        if(startDate != null && !startDate.isEmpty() && endDate != null && !endDate.isEmpty()) {
            pageData = orderRepository.findByCreatedDateBetween(LocalDateTime.parse(startDate), LocalDateTime.parse(endDate).plusDays(1), pageable);
        } else if(startDate != null && !startDate.isEmpty()) {
            pageData = orderRepository.findByCreatedDateAfter(LocalDateTime.parse(startDate), pageable);
        } else if(endDate != null && !endDate.isEmpty()) {
            System.out.println("masok");
            pageData = orderRepository.findByCreatedDateBefore(LocalDateTime.parse(endDate).plusDays(1), pageable);
        } else {
            pageData = orderRepository.findAll(pageable);
        }
        List<Order> result = pageData.getContent();
        int totalPages = Long.valueOf(pageData.getTotalElements()).intValue();

        return OrderPagingRes.builder()
                .totalPages(totalPages)
                .orders(result.stream().map(this::resConverter).collect(Collectors.toList()))
                .build();
    }

    public void updateOrder(Long id, OrderReq orderReq) throws DataNotFoundException {
        orderRepository.findById(id)
                .map(order -> {
                    order.setTotalPrice(orderReq.getTotalPrice());
                    order.setQuantity(orderReq.getQuantity());
                    orderRepository.save(order);
                    return order;
                })
                .orElseThrow(() -> new DataNotFoundException("Order not found"));
    }

    public void deleteOrder(Long id) {
        orderRepository.deleteById(id);
    }

    private Order reqConverter(OrderReq orderReq) {
        return Order.builder()
                .item(Item.builder().itemId(orderReq.getItemId()).build())
                .customer(Customer.builder().customerId(orderReq.getCustomerId()).build())
                .totalPrice(orderReq.getTotalPrice())
                .quantity(orderReq.getQuantity())
                .build();
    }

    private OrderRes resConverter(Order order) {
        return OrderRes.builder()
                .orderId(order.getOrderId())
                .orderCode(order.getOrderCode())
                .orderDate(order.getOrderDate())
                .totalPrice(order.getTotalPrice())
                .quantity(order.getQuantity())
                .customer(order.getCustomer())
                .item(order.getItem())
                .build();
    }
}
