package com.online.store.onlinestorebe.service;

import com.online.store.onlinestorebe.model.Order;
import com.online.store.onlinestorebe.repository.OrderRepository;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ReportService {
    @Autowired
    private OrderRepository orderRepository;

    public byte[] generateReport() throws FileNotFoundException, JRException {
        List<Order> orders = orderRepository.findAll();

        File file = ResourceUtils.getFile("classpath:Order.jrxml");
        JasperReport jasperReport = JasperCompileManager.compileReport(file.getAbsolutePath());
        JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(orders);
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("createdBy", "Riffs");
        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, dataSource);

        byte[] data = JasperExportManager.exportReportToPdf(jasperPrint);

        return data;
    }
}
