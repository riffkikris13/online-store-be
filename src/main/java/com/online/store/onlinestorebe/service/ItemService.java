package com.online.store.onlinestorebe.service;

import com.online.store.onlinestorebe.dto.*;
import com.online.store.onlinestorebe.exception.DataNotFoundException;
import com.online.store.onlinestorebe.model.Item;
import com.online.store.onlinestorebe.repository.ItemRepository;
import io.minio.*;
import io.minio.errors.*;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class ItemService {
    private ItemRepository itemRepository;

    private MinioClient minioClient;

    public ItemRes createItem(ItemReq itemReq) throws ServerException, InsufficientDataException, ErrorResponseException, IOException, NoSuchAlgorithmException, InvalidKeyException, InvalidResponseException, XmlParserException, InternalException {
        Item item = reqConverter(itemReq);
        item.setItemCode("ITM-" + UUID.randomUUID().toString().substring(0, 13));
        item.setIsAvailable(true);
        if(item.getStock() > 0) {
            item.setLastRestock(LocalDateTime.now());
        }

        if (!minioClient.bucketExists(BucketExistsArgs.builder().bucket("item-pict").build())) {
            minioClient.makeBucket(MakeBucketArgs.builder().bucket("item-pict").build());
        }

        String objName = System.currentTimeMillis() + "-" + item.getItemPic();

        minioClient.putObject(
                PutObjectArgs.builder()
                        .bucket("item-pict")
                        .object(objName)
                        .stream(itemReq.getItemPic().getInputStream(), itemReq.getItemPic().getSize(), -1)
                        .contentType(itemReq.getItemPic().getContentType())
                        .build()
        );

        String fileUrl = "http://localhost:9000" + "/" + "item-pict" + "/" + objName;
        item.setItemPic(fileUrl);

        return resConverter(itemRepository.save(item));
    }

    public ItemRes getItem(Long id) throws DataNotFoundException {
        return resConverter(itemRepository.findById(id).orElseThrow(() -> new DataNotFoundException("Item not found")));
    }

    public ItemPagingRes getItems(String itemName, int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        Page<Item> pageData;
        if(itemName != null && !itemName.isEmpty()) {
            pageData = itemRepository.findByItemNameContainingAndIsAvailable(itemName, true, pageable);
        } else {
            pageData = itemRepository.findByIsAvailable(true, pageable);
        }
        List<Item> result = pageData.getContent();
        int totalPages = Long.valueOf(pageData.getTotalElements()).intValue();

        return ItemPagingRes.builder()
                .totalPages(totalPages)
                .items(result.stream().map(this::resConverter).collect(Collectors.toList()))
                .build();
    }

    public void updateItem(Long id, ItemReq itemReq) throws DataNotFoundException {
        itemRepository.findById(id)
                .map(item -> {
                    item.setItemName(itemReq.getItemName());
                    item.setPrice(itemReq.getPrice());

                    if(!Objects.equals(item.getStock(), itemReq.getStock())) {
                        item.setStock(itemReq.getStock());
                        item.setLastRestock(LocalDateTime.now());
                    }

                    if(itemReq.getItemPic() != null && !itemReq.getItemPic().isEmpty()) {
                        String objName = System.currentTimeMillis() + "-" + itemReq.getItemPic().getOriginalFilename();

                        try {
                            if(!item.getItemPic().isEmpty()) {
                                minioClient.removeObject(RemoveObjectArgs.builder()
                                        .bucket("item-pict")
                                        .object(item.getItemPic())
                                        .build());
                            }
                            minioClient.putObject(
                                    PutObjectArgs.builder()
                                            .bucket("item-pict")
                                            .object(objName)
                                            .stream(itemReq.getItemPic().getInputStream(), itemReq.getItemPic().getSize(), -1)
                                            .contentType(itemReq.getItemPic().getContentType())
                                            .build()
                            );
                        } catch (Exception e) {
                            throw new RuntimeException(e);
                        }

                        String fileUrl = "http://localhost:9000" + "/" + "item-pict" + "/" + objName;
                        item.setItemPic(fileUrl);
                    }

                    itemRepository.save(item);
                    return item;
                })
                .orElseThrow(() -> new DataNotFoundException("Item not found"));
    }

    public void deleteItem(Long id) {
        Item item = itemRepository.findById(id).get();
        item.setIsAvailable(false);

        itemRepository.save(item);
    }

    private Item reqConverter(ItemReq itemReq) {
        return Item.builder()
                .itemName(itemReq.getItemName())
                .stock(itemReq.getStock())
                .price(itemReq.getPrice())
                .itemPic(itemReq.getItemPic() != null ? itemReq.getItemPic().getOriginalFilename() : "")
                .build();
    }

    private ItemRes resConverter(Item item) {
        return ItemRes.builder()
                .itemId(item.getItemId())
                .itemName(item.getItemName())
                .itemCode(item.getItemCode())
                .stock(item.getStock())
                .price(item.getPrice())
                .isAvailable(item.getIsAvailable())
                .lastRestock(item.getLastRestock())
                .itemPic(item.getItemPic())
                .build();
    }
}
