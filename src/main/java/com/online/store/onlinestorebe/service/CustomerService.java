package com.online.store.onlinestorebe.service;

import com.online.store.onlinestorebe.dto.CustomerPagingRes;
import com.online.store.onlinestorebe.dto.CustomerReq;
import com.online.store.onlinestorebe.dto.CustomerRes;
import com.online.store.onlinestorebe.exception.DataNotFoundException;
import com.online.store.onlinestorebe.model.Customer;
import com.online.store.onlinestorebe.repository.CustomerRepository;
import io.minio.*;
import io.minio.errors.*;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class CustomerService {
    private CustomerRepository customerRepository;

    private MinioClient minioClient;

    public CustomerRes createCustomer(CustomerReq customerReq) throws ServerException, InsufficientDataException, ErrorResponseException, IOException, NoSuchAlgorithmException, InvalidKeyException, InvalidResponseException, XmlParserException, InternalException {
        Customer customer = reqConverter(customerReq);
        customer.setCustomerCode("CST-" + UUID.randomUUID().toString().substring(0, 13));
        customer.setIsActive(true);

        if(!customer.getCustomerPic().isEmpty()) {
            if (!minioClient.bucketExists(BucketExistsArgs.builder().bucket("customer-pict").build())) {
                minioClient.makeBucket(MakeBucketArgs.builder().bucket("customer-pict").build());
            }

            String objName = System.currentTimeMillis() + "-" + customer.getCustomerPic();

            minioClient.putObject(
                    PutObjectArgs.builder()
                            .bucket("customer-pict")
                            .object(objName)
                            .stream(customerReq.getCustomerPic().getInputStream(), customerReq.getCustomerPic().getSize(), -1)
                            .contentType(customerReq.getCustomerPic().getContentType())
                            .build()
            );

            String fileUrl = "http://localhost:9000" + "/" + "customer-pict" + "/" + objName;
            customer.setCustomerPic(fileUrl);
        }

        return resConverter(customerRepository.save(customer));
    }

    public CustomerRes getCustomer(Long id) throws DataNotFoundException {
        return resConverter(customerRepository.findByCustomerIdAndIsActive(id, true).orElseThrow(() -> new DataNotFoundException("Customer not found")));
    }

    public CustomerPagingRes getCustomers(String customerName, int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        Page<Customer> pageData;
        if(customerName != null && !customerName.isEmpty()) {
            pageData = customerRepository.findByCustomerNameContainingAndIsActive(customerName, true, pageable);
        } else {
            pageData = customerRepository.findByIsActive(true, pageable);
        }
        List<Customer> result = pageData.getContent();
        int totalPages = Long.valueOf(pageData.getTotalElements()).intValue();

        return CustomerPagingRes.builder()
                .totalPages(totalPages)
                .customers(result.stream().map(this::resConverter).collect(Collectors.toList()))
                .build();
    }

    public void updateCustomer(Long id, CustomerReq customerReq) throws DataNotFoundException {
        customerRepository.findById(id)
                .map(customer -> {
                    customer.setCustomerName(customerReq.getCustomerName());
                    customer.setCustomerAddress(customerReq.getCustomerAddress());
                    customer.setCustomerPhone(customerReq.getCustomerPhone());

                    if(customerReq.getCustomerPic() != null && !customerReq.getCustomerPic().isEmpty()) {
                        String objName = System.currentTimeMillis() + "-" + customerReq.getCustomerPic().getOriginalFilename();

                        try {
                            if(!customer.getCustomerPic().isEmpty()) {
                                minioClient.removeObject(RemoveObjectArgs.builder()
                                        .bucket("customer-pict")
                                        .object(customer.getCustomerPic())
                                        .build());
                            }
                            minioClient.putObject(
                                    PutObjectArgs.builder()
                                            .bucket("customer-pict")
                                            .object(objName)
                                            .stream(customerReq.getCustomerPic().getInputStream(), customerReq.getCustomerPic().getSize(), -1)
                                            .contentType(customerReq.getCustomerPic().getContentType())
                                            .build()
                            );
                        } catch (Exception e) {
                            throw new RuntimeException(e);
                        }

                        String fileUrl = "http://localhost:9000" + "/" + "customer-pict" + "/" + objName;
                        customer.setCustomerPic(fileUrl);
                    }

                    customerRepository.save(customer);
                    return customer;
                })
                .orElseThrow(() -> new DataNotFoundException("Customer not found"));
    }

    public void deleteCustomer(Long id) {
        Customer customer = customerRepository.findById(id).get();
        customer.setIsActive(false);

        customerRepository.save(customer);
    }

    private Customer reqConverter(CustomerReq customerReq) {
        return Customer.builder()
                .customerName(customerReq.getCustomerName())
                .customerAddress(customerReq.getCustomerAddress())
                .customerPhone(customerReq.getCustomerPhone())
                .customerPic(customerReq.getCustomerPic() != null ? customerReq.getCustomerPic().getOriginalFilename() : "")
                .build();
    }

    private CustomerRes resConverter(Customer customer) {
        return CustomerRes.builder()
                .customerId(customer.getCustomerId())
                .customerName(customer.getCustomerName())
                .customerAddress(customer.getCustomerAddress())
                .customerCode(customer.getCustomerCode())
                .customerPhone(customer.getCustomerPhone())
                .isActive(customer.getIsActive())
                .lastOrderDate(customer.getLastOrderDate())
                .customerPic(customer.getCustomerPic())
                .build();
    }
}
