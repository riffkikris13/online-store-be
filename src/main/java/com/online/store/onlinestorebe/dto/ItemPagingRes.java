package com.online.store.onlinestorebe.dto;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class ItemPagingRes {
    private List<ItemRes> items;
    private int totalPages;
}
