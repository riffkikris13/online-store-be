package com.online.store.onlinestorebe.dto;

import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

@Data
public class ItemReq {
    @NotBlank(message = "Name is required")
    @Size(max = 50, message = "Name should not be more than 50 letters")
    private String itemName;

    @NotNull
    @Min(value = 0, message = "Stock cannot be less than 0")
    private Integer stock;

    @NotNull
    @Min(value = 0, message = "Price cannot be less than 0")
    private Integer price;

    private MultipartFile itemPic;
}
