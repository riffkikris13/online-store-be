package com.online.store.onlinestorebe.dto;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class CustomerPagingRes {
    private List<CustomerRes> customers;
    private int totalPages;
}
