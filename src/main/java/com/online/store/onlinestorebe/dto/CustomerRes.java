package com.online.store.onlinestorebe.dto;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;
@Data
@Builder
public class CustomerRes {
    private Long customerId;

    private String customerName;

    private String customerAddress;

    private String customerCode;

    private String customerPhone;

    private Boolean isActive;

    private LocalDateTime lastOrderDate;

    private String customerPic;
}
