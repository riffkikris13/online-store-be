package com.online.store.onlinestorebe.dto;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class OrderPagingRes {
    private List<OrderRes> orders;
    private int totalPages;
}
