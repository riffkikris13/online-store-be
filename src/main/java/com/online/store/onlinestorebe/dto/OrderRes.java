package com.online.store.onlinestorebe.dto;

import com.online.store.onlinestorebe.model.Customer;
import com.online.store.onlinestorebe.model.Item;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder
public class OrderRes {
    private Long orderId;

    private String orderCode;

    private LocalDateTime orderDate;

    private Integer totalPrice;

    private Integer quantity;

    private Customer customer;

    private Item item;
}
