package com.online.store.onlinestorebe.dto;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.Data;
import org.springframework.lang.Nullable;
import org.springframework.web.multipart.MultipartFile;

@Data
public class CustomerReq {
    @NotBlank(message = "Name is required")
    @Size(max = 50, message = "Name should not be more than 50 letters")
    private String customerName;

    @NotBlank(message = "Address is required")
    @Size(max = 150, message = "Address should not be more than 150 letters")
    private String customerAddress;

    @NotBlank(message = "Phone number is required")
    @Size(max = 30, message = "Phone number should not be more than 30 letters")
    private String customerPhone;

    @Nullable
    private MultipartFile customerPic;
}
