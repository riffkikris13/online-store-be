package com.online.store.onlinestorebe.dto;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder
public class ItemRes {
    private Long itemId;

    private String itemName;

    private String itemCode;

    private Integer stock;

    private Integer price;

    private Boolean isAvailable;

    private LocalDateTime lastRestock;

    private String itemPic;
}
