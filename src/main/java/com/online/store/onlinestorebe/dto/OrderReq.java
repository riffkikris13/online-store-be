package com.online.store.onlinestorebe.dto;

import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
public class OrderReq {
    private Long itemId;

    private Long customerId;

    @NotNull
    @Min(value = 0, message = "Total price cannot be less than 0")
    private Integer totalPrice;

    @NotNull
    @Min(value = 0, message = "Total price cannot be less than 0")
    private Integer quantity;
}
