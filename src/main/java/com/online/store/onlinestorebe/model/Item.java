package com.online.store.onlinestorebe.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Entity
@Table(name = "items")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Item {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "item_id")
    private Long itemId;

    @Column(name = "item_name")
    private String itemName;

    @Column(name = "item_code")
    private String itemCode;

    private Integer stock;

    private Integer price;

    @Column(name = "is_available")
    private Boolean isAvailable;

    @Column(name = "last_restock")
    private LocalDateTime lastRestock;

    @Column(name = "item_pic")
    private String itemPic;
}
