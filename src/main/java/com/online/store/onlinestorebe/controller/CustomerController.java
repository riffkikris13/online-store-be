package com.online.store.onlinestorebe.controller;

import com.online.store.onlinestorebe.dto.CustomerPagingRes;
import com.online.store.onlinestorebe.dto.CustomerReq;
import com.online.store.onlinestorebe.dto.CustomerRes;
import com.online.store.onlinestorebe.exception.DataNotFoundException;
import com.online.store.onlinestorebe.service.CustomerService;
import io.minio.errors.*;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

@RestController
@RequestMapping(path = "/customer")
@AllArgsConstructor
public class CustomerController {
    private CustomerService customerService;
    @PostMapping
    private ResponseEntity<CustomerRes> createCustomer(@Valid @ModelAttribute CustomerReq customerReq) throws ServerException, InsufficientDataException, ErrorResponseException, IOException, NoSuchAlgorithmException, InvalidKeyException, InvalidResponseException, XmlParserException, InternalException {
        CustomerRes customer = customerService.createCustomer(customerReq);
        return new ResponseEntity<>(customer, HttpStatus.CREATED);
    }

    @GetMapping("/detail/{id}")
    private ResponseEntity<CustomerRes> getCustomer(@PathVariable Long id) throws DataNotFoundException {
        return ResponseEntity.ok(customerService.getCustomer(id));
    }

    @GetMapping
    private ResponseEntity<CustomerPagingRes> getCustomers(@RequestParam("name") String name, @RequestParam("page") int page, @RequestParam("size") int size) throws DataNotFoundException {
        return ResponseEntity.ok(customerService.getCustomers(name, page, size));
    }

    @PatchMapping("/{id}")
    private ResponseEntity<Void> updateCustomer(@PathVariable Long id, @Valid @ModelAttribute CustomerReq customerReq) throws DataNotFoundException {
        customerService.updateCustomer(id, customerReq);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @DeleteMapping("/{id}")
    private ResponseEntity<Void> deleteCustomer(@PathVariable Long id) {
        customerService.deleteCustomer(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}
