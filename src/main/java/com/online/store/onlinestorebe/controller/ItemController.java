package com.online.store.onlinestorebe.controller;

import com.online.store.onlinestorebe.dto.*;
import com.online.store.onlinestorebe.exception.DataNotFoundException;
import com.online.store.onlinestorebe.service.ItemService;
import io.minio.errors.*;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

@RestController
@RequestMapping(path = "/item")
@AllArgsConstructor
public class ItemController {
    private ItemService itemService;

    @PostMapping
    private ResponseEntity<ItemRes> createItem(@Valid @ModelAttribute ItemReq itemReq) throws ServerException, InsufficientDataException, ErrorResponseException, IOException, NoSuchAlgorithmException, InvalidKeyException, InvalidResponseException, XmlParserException, InternalException {
        ItemRes item = itemService.createItem(itemReq);
        return new ResponseEntity<>(item, HttpStatus.CREATED);
    }

    @GetMapping("/detail/{id}")
    private ResponseEntity<ItemRes> getItem(@PathVariable Long id) throws DataNotFoundException {
        return ResponseEntity.ok(itemService.getItem(id));
    }

    @GetMapping
    private ResponseEntity<ItemPagingRes> getItems(@RequestParam("name") String name, @RequestParam("page") int page, @RequestParam("size") int size) throws DataNotFoundException {
        return ResponseEntity.ok(itemService.getItems(name, page, size));
    }

    @PatchMapping("/{id}")
    private ResponseEntity<Void> updateItem(@PathVariable Long id, @Valid @ModelAttribute ItemReq itemReq) throws DataNotFoundException {
        itemService.updateItem(id, itemReq);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @DeleteMapping("/{id}")
    private ResponseEntity<Void> deleteItem(@PathVariable Long id) {
        itemService.deleteItem(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
