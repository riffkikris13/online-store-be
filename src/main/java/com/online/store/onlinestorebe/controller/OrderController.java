package com.online.store.onlinestorebe.controller;

import com.online.store.onlinestorebe.dto.*;
import com.online.store.onlinestorebe.exception.DataNotFoundException;
import com.online.store.onlinestorebe.service.OrderService;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/order")
@AllArgsConstructor
public class OrderController {
    private OrderService orderService;

    @PostMapping
    private ResponseEntity<OrderRes> createOrder(@Valid @RequestBody OrderReq orderReq) throws DataNotFoundException {
        OrderRes order = orderService.createOrder(orderReq);
        return new ResponseEntity<>(order, HttpStatus.CREATED);
    }

    @GetMapping("/detail/{id}")
    private ResponseEntity<OrderRes> getOrder(@PathVariable Long id) throws DataNotFoundException {
        return ResponseEntity.ok(orderService.getOrder(id));
    }

    @GetMapping
    private ResponseEntity<OrderPagingRes> getCustomers(@RequestParam("startDate") String startDate, @RequestParam("endDate") String endDate, @RequestParam("page") int page, @RequestParam("size") int size) throws DataNotFoundException {
        return ResponseEntity.ok(orderService.getOrders(startDate, endDate, page, size));
    }

    @PatchMapping("/{id}")
    private ResponseEntity<Void> updateOrder(@PathVariable Long id, @Valid @RequestBody OrderReq orderReq) throws DataNotFoundException {
        orderService.updateOrder(id, orderReq);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @DeleteMapping("/{id}")
    private ResponseEntity<Void> deleteOrder(@PathVariable Long id) {
        orderService.deleteOrder(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
