CREATE TABLE customers (
    customer_id BIGINT AUTO_INCREMENT PRIMARY KEY,
    customer_name VARCHAR(50) NOT NULL,
    customer_address VARCHAR(150),
    customer_code VARCHAR(80) NOT NULL,
    customer_phone VARCHAR(30),
    is_active BIT(1) DEFAULT b'1',
    last_order_date TIMESTAMP,
    customer_pic VARCHAR(255),
    CONSTRAINT uniq_customer_code UNIQUE (customer_code)
);

CREATE TABLE items (
    item_id BIGINT AUTO_INCREMENT PRIMARY KEY,
    item_name VARCHAR(50) NOT NULL,
    item_code VARCHAR(80) NOT NULL,
    stock INT DEFAULT 0,
    price INT,
    is_available BIT(1) DEFAULT b'0',
    last_restock TIMESTAMP,
    item_pic VARCHAR(255),
    CONSTRAINT uniq_item_code UNIQUE (item_code)
);

CREATE TABLE orders (
    order_id BIGINT AUTO_INCREMENT PRIMARY KEY,
    order_code VARCHAR(80) NOT NULL,
    order_date TIMESTAMP,
    total_price INT,
    customer_id BIGINT NOT NULL,
    item_id BIGINT NOT NULL,
    quantity INT NOT NULL,
    CONSTRAINT fk_customers FOREIGN KEY (customer_id) REFERENCES customers(customer_id),
    CONSTRAINT fk_items FOREIGN KEY (item_id) REFERENCES items(item_id)
);